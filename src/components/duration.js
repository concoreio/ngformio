var fs = require('fs');

module.exports = function(app) {
  app.config([
    'formioComponentsProvider',
    function(formioComponentsProvider) {
      /*var isNumeric = function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
      };*/
      formioComponentsProvider.register('duration', {
        title: 'Duration',
        template: 'formio/components/duration.html',
        settings: {
          input: true,
          tableView: true,
          inputType: 'number',
          label: '',
          key: 'durationField',
          placeholder: '',
          protected: false,
          persistent: true,
          fields: {
            days: {
              required: false,
              show: true
            },
            hours: {
              required: false,
              show: true
            },
            minutes: {
              required: false,
              show: true
            },
            seconds: {
              required: false,
              show: true
            }
          }
        },
        controller: ['$scope', function($scope) {
          if ($scope.builder) return; // FOR-71 - Skip parsing input data.

          $scope.data[$scope.component.key] = {
            days: undefined,
            hours: undefined,
            minutes: undefined,
            seconds: undefined
          };

          // Ensure that values are numbers.
          /* Object.keys($scope.data[$scope.component.key]).forEach(function(item) {
            if ($scope.data.hasOwnProperty($scope.component.key) && isNumeric($scope.data[$scope.component.key][item])) {
              $scope.data[$scope.component.key][item] = parseFloat($scope.data[$scope.component.key][item]);
            }
          });*/
        }]
      });
    }
  ]);

  app.run([
    '$templateCache',
    'FormioUtils',
    function($templateCache, FormioUtils) {
      $templateCache.put('formio/components/duration.html', FormioUtils.fieldWrap(
        fs.readFileSync(__dirname + '/../templates/components/duration.html', 'utf8')
      ));
    }
  ]);
};
