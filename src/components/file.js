/*eslint no-console: off*/
var fs = require('fs');
module.exports = function(app) {
  app.config([
    'formioComponentsProvider',
    function(formioComponentsProvider) {
      formioComponentsProvider.register('file', {
        title: 'File',
        template: 'formio/components/file.html',
        group: 'advanced',
        settings: {
          input: true,
          tableView: true,
          label: '',
          key: 'file',
          image: false,
          imageSize: '200',
          placeholder: '',
          multiple: false,
          defaultValue: '',
          protected: false,
          persistent: true,
          uploadClass: {},
          extensions: ['*'],
          validate: {
            required: false
          }
        },
        controller: ['$scope', function FileController($scope) {
          if ($scope.builder) return;
          $scope.itemFile = $scope.itemFile ? $scope.itemFile : {};
          $scope.data[$scope.component.key] = $scope.data[$scope.component.key] ? $scope.data[$scope.component.key] : undefined;
          $scope.itemFile[$scope.component.key] = {
            upload: false,
            listFiles: [],
            errorFile: false,
            extensions: []
          };

          var mimeTypes = {
            doc: 'application/msword',
            docx: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            gif: 'image/gif',
            gz: 'application/x-gzip',
            jpg: 'image/jpeg',
            png: 'image/png',
            pdf: 'application/pdf',
            xls: 'application/vnd.ms-excel',
            zip: 'application/zip'
          };

          if ($scope.component.extensions.length) {
            $scope.component.extensions.forEach(function(type) {
              if (type !== '*') {
                $scope.itemFile[$scope.component.key].extensions.push(mimeTypes[type]);
              }
              else {
                $scope.itemFile[$scope.component.key].extensions.push('*');
              }
            });
          }

          $scope.itemFile[$scope.component.key].extensions = $scope.itemFile[$scope.component.key].extensions.join(',');

          $scope.removeFile = function(f) {
            $scope.itemFile[$scope.component.key].listFiles.forEach(function(item, key) {
              if (f.name === $scope.itemFile[$scope.component.key].listFiles[key].name) {
                f.destroy();

                if ($scope.component.multiple) {
                  $scope.data[$scope.component.key].forEach(function(fileData, keyData) {
                    if (fileData.name === f.name) {
                      $scope.data[$scope.component.key].splice(keyData, 1);
                    }
                  });
                }
                else {
                  $scope.data[$scope.component.key] = null;
                }

                $scope.itemFile[$scope.component.key].listFiles.splice(key, 1);
                $scope.itemFile[$scope.component.key].upload.done -= 1;
              }
            });

            if (!$scope.itemFile[$scope.component.key].upload.done) {
              $scope.itemFile[$scope.component.key].upload = false;
            }

            console.log($scope.itemFile);
          };

          $scope.$watch(function() {
            return $scope.data[$scope.component.key];
          }, function() {
            console.log($scope.data[$scope.component.key]);
            if ($scope.component.multiple) {
              var files = $scope.data[$scope.component.key];
              Object.keys(files).forEach(function(f) {
                $scope.itemFile[$scope.component.key].listFiles.push(files[f]);
             });
            }
            else {
              if ($scope.data[$scope.component.key]) {
                if (!$scope.itemFile[$scope.component.key].listFiles.length) {
                  $scope.itemFile[$scope.component.key].listFiles = [$scope.data[$scope.component.key]];
                }
              }
            }
          });

          $scope.uploadFiles = function(files, error) {
            if (error.length) {
              $scope.itemFile[$scope.component.key].errorFile = error;
              return false;
            }

            if (!files.length) {
              return false;
            }

            /*if (!$scope.component.multiple && !$scope.data[$scope.component.key].length) {
              $scope.data[$scope.component.key] = [];
              $scope.itemFile[$scope.component.key].listFiles = [];
            }*/

            var UploadClass = $scope.component.uploadClass;
            $scope.files = files;
            $scope.itemFile[$scope.component.key].error = error;
            $scope.itemFile[$scope.component.key].upload = {
              qtd: files.length,
              done: 0,
              message:'Aguarde, processando upload'
            };

            angular.forEach(files, function(file) {
              $scope.itemFile[$scope.component.key].errorFile = false;
              var up = new UploadClass(file.name, file);
              up.save()
                .then(function(response) {
                  if ($scope.component.multiple) {
                    $scope.data[$scope.component.key].push(response.data);
                  }
                  else {
                    $scope.data[$scope.component.key] = response.data;
                  }
                  file.url = response.data.url;
                  file._class = response.data._class;
                  $scope.itemFile[$scope.component.key].listFiles.push(file);
                  $scope.itemFile[$scope.component.key].upload.qtd -= 1;
                  $scope.itemFile[$scope.component.key].upload.done += 1;
                  $scope.$apply();
                });
            });
          };
        }],
        viewTemplate: 'formio/componentsView/file.html'
      });
    }
  ]);

  app.run([
    '$templateCache',
    function(
      $templateCache
    ) {
      $templateCache.put('formio/components/file.html',
        fs.readFileSync(__dirname + '/../templates/components/file.html', 'utf8')
      );
    }
  ]);
};
