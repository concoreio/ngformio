/*eslint no-console: off*/
var fs = require('fs');
module.exports = function(app) {
  /*jshint camelcase: false */
  app.config([
    'formioComponentsProvider',
    function(formioComponentsProvider) {
      formioComponentsProvider.register('url', {
        title: 'Address',
        template: function() {
          return 'formio/components/url.html';
        },
        controller: ['$scope', '$http', function($scope, $http) {
          if ($scope.builder) return;
          $scope.address = {};
          $scope.addresses = [];
          $scope.links = {};
          $scope.data[$scope.component.key] = $scope.data[$scope.component.key] ? $scope.data[$scope.component.key] : [];
          $scope.currentLink = '';
          $scope.regex = RegExp('^((https?|ftp)://)?([a-z]+[.])?[a-z0-9-]+([.][a-z]{1,4}){1,2}(/.*[?].*)?$', 'i');

          $scope.urlChange = function(address) {
            console.log($scope.data[$scope.component.key]);
            if (typeof address.url === 'undefined') {
              return false;
            }

            if ($scope.component.multiple) {
              $scope.data[$scope.component.key].push(address.url);
            }
            else {
              $scope.links = {};
              $scope.data[$scope.component.key] = address.url;
            }

            $scope.links[address.url] = [];
            $scope.links[address.url].push(address);

            $scope.addresses = [{title: ''}];
          };

          var requestApi = function(params) {
            return $http.get('https://api.concore.io/api/metadata', {
              disableJWT: true,
              headers: {
                Authorization: undefined,
                Pragma: undefined,
                'Cache-Control': undefined
              },
              params: {
                uri: params.address
              }
            });
          };

          $scope.$watch(function() {
            return $scope.data[$scope.component.key];
          }, function() {
            if (!$scope.data[$scope.component.key].length || $scope.data[$scope.component.key] === '') {
              return;
            }

            if ($scope.component.multiple) {
              if ($scope.data[$scope.component.key].length) {
                $scope.data[$scope.component.key].forEach(function(url) {
                  var params = {
                    address: url
                  };
                  requestApi(params).then(function(response) {
                    var link = url;
                    $scope.links[link] = [];
                    response.data.url = link;
                    $scope.links[link].push(response.data);
                  });
                });
              }
            }
            else {
              var url = $scope.data[$scope.component.key];
              var params = {
                address: $scope.data[$scope.component.key]
              };
              requestApi(params).then(function(response) {
                var link = url;
                $scope.links[link] = [];
                response.data.url = link;
                $scope.links[link].push(response.data);
              });
            }
          });

          $scope.urlRemove = function(address) {
            if ($scope.component.multiple) {
              $scope.data[$scope.component.key].forEach(function(item, key) {
                if (item === address) {
                  $scope.data[$scope.component.key].splice(key, 1);
                  delete $scope.links[address];
                  return;
                }
              });
            }
            else {
              $scope.data[$scope.component.key] = [];
              delete $scope.links[address];
            }
          };

          $scope.refreshAddress = function(address) {
            var params = {
              address: address
            };

            if (!address) {
              return;
            }
            $scope.addresses = [{title: 'carregando...'}];
            return requestApi(params).then(function(response) {
              response.data.url = params.address;
              response.data.title = response.data.title ? response.data.title : 'Home page from address ' + params.address;
              $scope.addresses = [response.data];
              console.log($scope.addresses);
            }).catch(function() {
              $scope.addresses = [{title: 'Endereço não encontrado!'}];
            });
          };
        }],
        tableView: function(data) {
          return data ? data.description : '';
        },
        group: 'advanced',
        settings: {
          input: true,
          tableView: true,
          label: '',
          key: 'addressField',
          placeholder: '',
          multiple: false,
          protected: false,
          unique: false,
          persistent: true,
          map: {
            region: '',
            key: ''
          },
          validate: {
            required: false
          }
        }
      });
    }
  ]);
  app.run([
    '$templateCache',
    function($templateCache) {
      $templateCache.put('formio/components/url.html',
        fs.readFileSync(__dirname + '/../templates/components/url.html', 'utf8')
      );
    }
  ]);
};
